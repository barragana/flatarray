module.exports = function flat(array, result = []) {
  array.forEach(function(item) {
    if (Array.isArray(item)) {
      flat(item, result);
    } else {
      result.push(item);
    }
  });
  return result;
}
