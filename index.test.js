const flatArray = require('./index');

describe('flatArray should', () => {
  it('return flat array', () => {
    expect(flatArray([[1,2,[3]],4])).toEqual([1, 2, 3, 4]);
    expect(flatArray([1,2, 3, 4])).toEqual([1, 2, 3, 4]);
    expect(flatArray([[1,2,[3]],[[[[4]]]]])).toEqual([1, 2, 3, 4]);
  })
});
